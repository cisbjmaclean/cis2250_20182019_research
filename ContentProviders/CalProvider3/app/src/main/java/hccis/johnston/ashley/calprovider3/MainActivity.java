package hccis.johnston.ashley.calprovider3;

import android.Manifest;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.icu.text.DateFormat;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.net.Uri;
import android.provider.CalendarContract;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int MY_PERMISSIONS_REQUEST_READ_WRITE_CALENDAR = 456;
    Button buttonGetCalPermissions;
    Button buttonQueryACalendar;
    Button buttonAddEvent;
    Button buttonQueryEvent;
    Button buttonIntent;


    /////////////Query a calendar////////////////////
// Projection array. Creating indices for this array instead of doing
// dynamic lookups improves performance.
    public static final String[] EVENT_PROJECTION = new String[] {
            CalendarContract.Calendars._ID,                           // 0
            CalendarContract.Calendars.ACCOUNT_NAME,                  // 1
            CalendarContract.Calendars.CALENDAR_DISPLAY_NAME,         // 2
            CalendarContract.Calendars.OWNER_ACCOUNT                  // 3
    };

    // The indices for the projection array above.
    private static final int PROJECTION_ID_INDEX = 0;
    private static final int PROJECTION_ACCOUNT_NAME_INDEX = 1;
    private static final int PROJECTION_DISPLAY_NAME_INDEX = 2;
    private static final int PROJECTION_OWNER_ACCOUNT_INDEX = 3;
    /////////////Query a calendar////////////////////


    /////////////Query an Event////////////////////
    public static final String[] INSTANCE_PROJECTION = new String[] {
            CalendarContract.Instances.EVENT_ID,      // 0
            CalendarContract.Instances.BEGIN,         // 1
            CalendarContract.Instances.TITLE          // 2
    };

    // The indices for the projection array above.
//    private static final int PROJECTION_ID_INDEX = 0;
    private static final int PROJECTION_BEGIN_INDEX = 1;
    private static final int PROJECTION_TITLE_INDEX = 2;

    /////////////Query an Event////////////////////



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonGetCalPermissions = findViewById(R.id.buttonGetCalPermissions);
        buttonGetCalPermissions.setOnClickListener(this);
        buttonAddEvent = findViewById(R.id.buttonAddEvent);
        buttonAddEvent.setOnClickListener(this);
        buttonIntent = findViewById(R.id.buttonIntent);
        buttonIntent.setOnClickListener(this);
        buttonQueryACalendar = findViewById(R.id.buttonQueryCalendar);
        buttonQueryACalendar.setOnClickListener(this);
        buttonQueryEvent = findViewById(R.id.buttonQueryEvent);
        buttonQueryEvent.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonGetCalPermissions:
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR}, MY_PERMISSIONS_REQUEST_READ_WRITE_CALENDAR);
                break;

            case R.id.buttonQueryCalendar:
// Run query
                Cursor cur = null;
                ContentResolver cr = getContentResolver();
                Uri uri = CalendarContract.Calendars.CONTENT_URI;
                String selection = "((" + CalendarContract.Calendars.ACCOUNT_NAME + " = ?) AND ("
                        + CalendarContract.Calendars.ACCOUNT_TYPE + " = ?) AND ("
                        + CalendarContract.Calendars.OWNER_ACCOUNT + " = ?))";
                String[] selectionArgs = new String[] {"hera@example.com", "com.example",
                        "hera@example.com"};
// Submit the query and get a Cursor object back.
                cur = cr.query(uri, EVENT_PROJECTION, null, null, null);

                // Use the cursor to step through the returned records
                while (cur.moveToNext()) {
                    long calID = 0;
                    String displayName = null;
                    String accountName = null;
                    String ownerName = null;

                    // Get the field values
                    calID = cur.getLong(PROJECTION_ID_INDEX);
                    displayName = cur.getString(PROJECTION_DISPLAY_NAME_INDEX);
                    accountName = cur.getString(PROJECTION_ACCOUNT_NAME_INDEX);
                    ownerName = cur.getString(PROJECTION_OWNER_ACCOUNT_INDEX);

                    // Do something with the values...
                    String output = calID+displayName+accountName+ownerName;

                    Toast.makeText(this, output, Toast.LENGTH_LONG).show();
                    Log.d("AJ", output + "103");

                }

                break;

            case R.id.buttonAddEvent:
                //Not working
                long calID = 4;
                long startMillis = 0;
                long endMillis = 0;
                Calendar beginTime = Calendar.getInstance();
                beginTime.set(2019, 1, 6, 7, 30);
                startMillis = beginTime.getTimeInMillis();
                Calendar endTime = Calendar.getInstance();
                endTime.set(2019, 1, 6, 8, 45);
                endMillis = endTime.getTimeInMillis();

                ContentResolver cr1 = getContentResolver();
                ContentValues values = new ContentValues();
                values.put(CalendarContract.Events.DTSTART, startMillis);
                values.put(CalendarContract.Events.DTEND, endMillis);
                values.put(CalendarContract.Events.TITLE, "Jazzercise");
                values.put(CalendarContract.Events.DESCRIPTION, "Group workout");
                values.put(CalendarContract.Events.CALENDAR_ID, calID);
                values.put(CalendarContract.Events.EVENT_TIMEZONE, "America/Los_Angeles");
                Uri uri1 = cr1.insert(CalendarContract.Events.CONTENT_URI, values);

// get the event ID that is the last element in the Uri
                long eventID = Long.parseLong(uri1.getLastPathSegment());
                Log.d("AJ", eventID+"");
                Toast.makeText(this, eventID+ "", Toast.LENGTH_LONG).show();

                break;
            case R.id.buttonQueryEvent:


// Specify the date range you want to search for recurring
// event instances
                Calendar beginTime1 = Calendar.getInstance();
                beginTime1.set(2011, 9, 23, 8, 0);
                long startMillis1 = beginTime1.getTimeInMillis();
                Calendar endTime1 = Calendar.getInstance();
                endTime1.set(2019, 10, 24, 8, 0);
                long endMillis1 = endTime1.getTimeInMillis();

                Cursor cur1 = null;
                ContentResolver cr2 = getContentResolver();

// The ID of the recurring event whose instances you are searching
// for in the Instances table
                String selection1 = CalendarContract.Instances.EVENT_ID + " = ?";
                String[] selectionArgs1 = new String[] {"5"};

// Construct the query with the desired date range.
                Uri.Builder builder = CalendarContract.Instances.CONTENT_URI.buildUpon();
                ContentUris.appendId(builder, startMillis1);
                ContentUris.appendId(builder, endMillis1);

// Submit the query
                cur1 =  cr2.query(builder.build(),
                        INSTANCE_PROJECTION,
                        null, null,
                        null);

                while (cur1.moveToNext()) {
                    String title = null;
                    long eventID1 = 0;
                    long beginVal = 0;

                    // Get the field values
                    eventID1 = cur1.getLong(PROJECTION_ID_INDEX);
                    beginVal = cur1.getLong(PROJECTION_BEGIN_INDEX);
                    title = cur1.getString(PROJECTION_TITLE_INDEX);

                    // Do something with the values.
                    Log.d("AJ", "Event:  " + title);
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTimeInMillis(beginVal);
                    DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
                    Log.d("AJ", "Date: " + formatter.format(calendar.getTime()));
                }

                break;
            case R.id.buttonIntent:
                Calendar beginTime2 = Calendar.getInstance();
                beginTime2.set(2012, 0, 19, 7, 30);
                Calendar endTime2 = Calendar.getInstance();
                endTime2.set(2012, 0, 19, 8, 30);
                Intent intent = new Intent(Intent.ACTION_INSERT)
                        .setData(CalendarContract.Events.CONTENT_URI)
                        .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime2.getTimeInMillis())
                        .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime2.getTimeInMillis())
                        .putExtra(CalendarContract.Events.TITLE, "Yoga")
                        .putExtra(CalendarContract.Events.DESCRIPTION, "Group class")
                        .putExtra(CalendarContract.Events.EVENT_LOCATION, "The gym")
                        .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY)
                        .putExtra(Intent.EXTRA_EMAIL, "rowan@example.com,trevor@example.com");
                startActivity(intent);
                break;
        }
    }
}
