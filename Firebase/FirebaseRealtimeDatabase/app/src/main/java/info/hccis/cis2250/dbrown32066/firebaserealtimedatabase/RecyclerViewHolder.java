package info.hccis.cis2250.dbrown32066.firebaserealtimedatabase;

/**
 * Small recycler view which is like the middle man between the
 * posts received from the database and the recycler view component.
 *
 * Author: Darcy Brown
 * Date: January 26th, 2019
 */

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class RecyclerViewHolder extends RecyclerView.ViewHolder {

    public TextView txt_title, txt_content;

    public RecyclerViewHolder(@NonNull View itemView) {
        super(itemView);

        txt_title = (TextView) itemView.findViewById(R.id.txt_title);
        txt_content = (TextView) itemView.findViewById(R.id.txt_Content);
    }


}
