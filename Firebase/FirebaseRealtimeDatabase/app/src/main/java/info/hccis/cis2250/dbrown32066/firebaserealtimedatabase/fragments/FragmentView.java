package info.hccis.cis2250.dbrown32066.firebaserealtimedatabase.fragments;
/**
 * This is the Fragment View fragment. This fragment has all the methods which pertain
 * to controlling viewing the posts from the database.
 *
 * It uses the Static Firebase components declared in the Main Activity.
 *
 * Author: Darcy Brown
 * Date: January 26th, 2019
 */
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;

import info.hccis.cis2250.dbrown32066.firebaserealtimedatabase.MainActivity;
import info.hccis.cis2250.dbrown32066.firebaserealtimedatabase.R;
import info.hccis.cis2250.dbrown32066.firebaserealtimedatabase.RecyclerViewHolder;
import info.hccis.cis2250.dbrown32066.firebaserealtimedatabase.entity.Post;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentView.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentView#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentView extends Fragment{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    RecyclerView recyclerView;
    public FragmentView() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentView.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentView newInstance(String param1, String param2) {
        FragmentView fragment = new FragmentView();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    /**
     *
     * This method does all the UI initialization of the recycler view imported in the
     * Gradle file, and the text views from the post_item.xml layout for the recycler
     * view. It uses the FirebaseRecyclerOptions from the FirebaseUI imported in the
     * Gradle file as well as the FirebaseRecylerAdapter from the same import. Both
     * declared in the main activity.
     *
     * The recycler view holder is a small file which extends RecyclerView.ViewHolder and
     * is used in the adapter below.
     */
    @Override
    public void onStart() {
        super.onStart();

        // Initialize Component
        recyclerView = (RecyclerView) this.getView().findViewById(R.id.recycler_View);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        TextView txtTitle = (TextView) getView().findViewById(R.id.txt_title);
        TextView txtContent = (TextView) getView().findViewById(R.id.txt_Content);


        // Use FirebaseUI Recycler Options function and builder to get the posts.
        MainActivity.options = new FirebaseRecyclerOptions.Builder<Post>()
                .setQuery(MainActivity.databaseReference, Post.class)
                .build();


        MainActivity.adapter =
                new FirebaseRecyclerAdapter<Post, RecyclerViewHolder>(MainActivity.options) {
                    // These actions are done for all instances of a Post class in the
                    // Firebase Database.
                    @Override
                    protected void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position, @NonNull Post model) {
                        // Set the RecyclerViewHolder text to the retrieved text
                        holder.txt_title.setText(model.getTitle());
                        holder.txt_content.setText(model.getContent());
                    }

                    @NonNull
                    @Override
                    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                        // Make a view with the LayoutInflater method using our post_item.xml layout
                        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.post_item, viewGroup, false);
                        // Return a new RecyclerViewObject with our itemView.
                        return new RecyclerViewHolder(itemView);
                    }
                };

        // Make the adapter start listening and set the recycler view adapter
        // to the MainActivity.adapter.
        MainActivity.adapter.startListening();
        recyclerView.setAdapter(MainActivity.adapter);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_view, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
