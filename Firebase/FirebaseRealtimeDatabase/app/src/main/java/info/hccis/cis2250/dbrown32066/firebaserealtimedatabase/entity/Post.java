package info.hccis.cis2250.dbrown32066.firebaserealtimedatabase.entity;

/**
 * A Standard post object which has all the methods and attributes which pertain
 * to a post.
 *
 * Author: Darcy Brown
 * Date: January 26th, 2019
 */
public class Post {
    private String title, content;

    public Post(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public Post() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
