package info.hccis.cis2250.dbrown32066.firebaserealtimedatabase;

import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.support.v4.app.Fragment;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import info.hccis.cis2250.dbrown32066.firebaserealtimedatabase.entity.Post;
import info.hccis.cis2250.dbrown32066.firebaserealtimedatabase.fragments.FragmentAdd;
import info.hccis.cis2250.dbrown32066.firebaserealtimedatabase.fragments.FragmentView;

public class MainActivity extends AppCompatActivity implements
        FragmentAdd.OnFragmentInteractionListener,
        FragmentView.OnFragmentInteractionListener{


    // Blank fragment to assign my new fragments to.
    Fragment newFragment = new Fragment();

    // Declare Firebase Database and Database Reference for the program
    public static FirebaseDatabase firebaseDatabase;
    public static DatabaseReference databaseReference;
    // Declare FirebaseUI options and adapter instances.
    public static FirebaseRecyclerOptions<Post> options;
    public static FirebaseRecyclerAdapter<Post, RecyclerViewHolder> adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialize Firebase
        FirebaseApp.initializeApp(this);
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("AndroidAppFirebase");

        setContentView(R.layout.activity_main);

        // Declare Buttons
        Button btnPost = (Button) findViewById(R.id.btn_Post);
        Button btnView = (Button) findViewById(R.id.btn_View);

        // Click Listeners to Fragments
        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Add Fragment
                newFragment = new FragmentAdd();
                initializeFragment();
            }
        });

        btnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newFragment = new FragmentView();
                initializeFragment();
            }
        });



    }

    public void initializeFragment(){
        // Declare a fragment transaction
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        // Replace the contents of the container with the new fragment
        ft.replace(R.id.fragment, newFragment);
        // or ft.add(R.id.your_placeholder, new FooFragment());
        // Complete the changes added above
        ft.commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
