package info.hccis.cis2250.dbrown32066.firebaserealtimedatabase.fragments;
/**
 * This is the Fragment Add fragment. This fragment has all the methods which pertain
 * to controlling adding a post to the database.
 *
 * It uses the Static Firebase components declared in the Main Activity.
 *
 * Author: Darcy Brown
 * Date: January 26th, 2019
 */
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import info.hccis.cis2250.dbrown32066.firebaserealtimedatabase.MainActivity;
import info.hccis.cis2250.dbrown32066.firebaserealtimedatabase.R;
import info.hccis.cis2250.dbrown32066.firebaserealtimedatabase.entity.Post;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentAdd.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentAdd#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentAdd extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    // Declare UI Components
    EditText txt_Title, txt_Content;
    Button btn_Post;

    public FragmentAdd() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentAdd.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentAdd newInstance(String param1, String param2) {
        FragmentAdd fragment = new FragmentAdd();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    /**
     *
     * This method relies on the databaseReference declaration from the MainActivity.
     * I declared them static so i could use them in multiple fragments. It also uses
     * the Post object. and a Snack Bar which had to be Imported in the gradle file.
     *
     *
     */
    private void postComment(){
        // Get the input data
        String title = txt_Title.getText().toString();
        String content = txt_Content.getText().toString();


        // Make a post instance
        Post post = new Post(title, content);

        // This will push the post item to the database with a unique id
        MainActivity.databaseReference.push() // Use this command to create unique ID
                .setValue(post); // Set the value of the post.


        // Make a Snack Bar to show success.
        Snackbar.make(getView(), "Post Successful!", Snackbar.LENGTH_SHORT)
                .setAction("Firebase Database Post", null).show();

        // Clear the boxes
        txt_Title.setText("");
        txt_Content.setText("");
    }

    /**
     *
     * Initializes the UI elements and button click listener.
     * Calls the postComment function above.
     *
     */
    @Override
    public void onStart() {
        super.onStart();
        // Assign UI Components
        txt_Title = (EditText) getView().findViewById(R.id.txt_EditTitle);
        txt_Content = (EditText) getView().findViewById(R.id.txt_AddContent);
        btn_Post = (Button) getView().findViewById(R.id.btn_Post);

        // On post click postComment function call.
        btn_Post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Post Comment Function
                postComment();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
