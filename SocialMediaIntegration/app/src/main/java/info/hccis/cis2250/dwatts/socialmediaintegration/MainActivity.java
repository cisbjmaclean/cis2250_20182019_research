package info.hccis.cis2250.dwatts.socialmediaintegration;
/*
    Shows off some social media integration with buttons
    Darcy Watts
    2018/01/28
 */
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Setting up variables
        final String message;
        final EditText editTextMessge = (EditText) findViewById(R.id.editTextMessage);
        Button facebook = (Button) findViewById(R.id.facebook);
        Button tweet = (Button) findViewById(R.id.Tweet);
        Button share = (Button) findViewById(R.id.share);


        //When the facebook button is clicked an intent is made to take the user to facebook.com
        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    intent= new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com"));
                    startActivity(intent);
            }
        });


        //When the share button is clicked it gathers up the information in the text box and sends
        //it to either the messaging app or to gmail
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message= editTextMessge.getText().toString();

                intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT,message);
                startActivity(intent.createChooser(intent,"Share via"));
            }
        });

        //When the tweet button is clicked it will take the user to twitter with a tweet prefilled
        tweet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message= editTextMessge.getText().toString();
                // Create intent using ACTION_VIEW and a normal Twitter url:
                String tweetUrl = String.format("https://twitter.com/intent/tweet?text=%s&url=%s",
                        urlEncode(message),
                        urlEncode(""));
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(tweetUrl));

 //Narrow down to official Twitter app, if available:
                List<ResolveInfo> matches = getPackageManager().queryIntentActivities(intent, 0);
                for (ResolveInfo info : matches) {
                    if (info.activityInfo.packageName.toLowerCase().startsWith("com.twitter")) {
                        intent.setPackage(info.activityInfo.packageName);
                    }
                }
                startActivity(intent);
            }
        });
    }

    //Encodes the URL
    public static String urlEncode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        }
        catch (UnsupportedEncodingException e) {
            Log.wtf("DWTest", "UTF-8 should always be supported", e);
            throw new RuntimeException("URLEncoder.encode() failed for " + s);
        }
    }

}
