/*
 * Student: Arman Alfonso
 * Course: CIS2250
 * Research: Kotlin interoperable with Java.
 *          Converted the FragmentAlfonsoArman3.java to FragmentAlfonsoArman3.kt
 *          All other files are in java.  Java can call .kt files and be able to run it.
 *
 */
package cis2250.hccis.info.fragmentapp.fragment

import android.content.Context
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.SeekBar
import android.widget.Switch
import android.widget.TextClock
import android.widget.TextView
import android.widget.Toast

import cis2250.hccis.info.fragmentapp.FragmentMacLeanBJ
import cis2250.hccis.info.fragmentapp.MainActivity
import cis2250.hccis.info.fragmentapp.R


class FragmentAlfonsoArman3 : Fragment() {


    private var mListener: OnFragmentInteractionListener? = null



    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater!!.inflate(R.layout.fragment_alfonso_arman3, container, false)
        val timeZones = arrayOf("Select Time Zone", "Canada/Atlantic",
                "Canada/Eastern", "Canada/Central", "Canada/Mountain", "Canada/Pacific")

        val listView: ListView
        listView = view.findViewById<View>(R.id.listViewZone) as ListView

        val listViewAdapter = ArrayAdapter(
                activity,
                android.R.layout.simple_list_item_1,
                timeZones
        )

        //This will show a dropdown for timezones
        listView.adapter = listViewAdapter

        return view

    }


    fun onButtonPressed(uri: Uri) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(uri)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun onStart() {
        super.onStart()

        //widget 1 - the switch will change the background color to either Red or Blue

        val switchColor = view!!.findViewById<View>(R.id.switchBackground) as Switch
        val tc = view!!.findViewById<View>(R.id.textClock1) as TextClock

        switchColor.setOnClickListener {
            if (switchColor.isChecked) {
                view!!.setBackgroundColor(Color.RED)
                switchColor.text = "Change to BLUE"
            } else {
                view!!.setBackgroundColor(Color.BLUE)
                switchColor.text = "Change to Red"
            }
        }

        //widget 2 ListView and 3 TextClock
        // ListView tzone will allow selection of time zones
        // TextClock textClock1 will be changed based on the tzone selection

        val tzone = view!!.findViewById<View>(R.id.listViewZone) as ListView
        val labelForClock = view!!.findViewById<TextView>(R.id.labelForClock)

        tzone.isClickable = true
        tzone.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
            val x = tzone.getItemAtPosition(position)

            //save selection
            val selected = x as String
            if (selected == "Select Time Zone") {
                //do nothing
            } else {
                //change timezone based on selection
                labelForClock.text = "Current Time: $selected"
                tc.timeZone = selected
            }
        }
    }


    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {

        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"


        fun newInstance(param1: String, param2: String): FragmentAlfonsoArman3 {
            val fragment = FragmentAlfonsoArman3()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}
