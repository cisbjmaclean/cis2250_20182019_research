package info.hccis.cis2250.kotlinwatercalculator

class CalculatorKotlin(_diameter: Double = 0.0, _height: Double = 0.0, _flowRate: Double = 0.0, _volume: Double = 0.0, _containerFillTime: Double = 0.0) {

    val diameter = _diameter
    val height = _height
    val flowRate = _flowRate
    var volume = _volume
    var containerFillTime = _containerFillTime

    init{
        calculate()
    }

    fun calculate(){
        calculateVolume()
        calculateTimeToFill()
    }

    fun calculateVolume(){
        volume = Math.pow(diameter / 2.0, 2.0) * Math.PI * height
    }

    fun calculateTimeToFill(){
        containerFillTime = volume * (flowRate / 61.0)
    }

}


