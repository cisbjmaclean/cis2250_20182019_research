package info.hccis.cis2250.kotlinwatercalculator

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import android.view.Menu
import android.view.MenuItem

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import java.math.RoundingMode
import java.text.DecimalFormat

class MainActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

        //Set a button on-click listener
        buttonCalculate.setOnClickListener{

            //Get the values from the user
            val diameter = editTextDiameter.text.toString().toDouble()
            val height = editTextHeight.text.toString().toDouble()
            val flowRate = editTextFlowRate.text.toString().toDouble()

            //Create calculator object using Kotlin class
            val calculator = CalculatorKotlin(diameter, height, flowRate)
            //Create calculator object using Java class
            //val calculator2 = CalculatorJava(diameter, height, flowRate)

            //Round the fill time
            var fillTime = calculator.containerFillTime
            val fillTimeRounded = "%.2f".format(fillTime)

            //Display the results
            textViewResult.text = fillTimeRounded
            //textViewResult.text = "Time to fill: ${fillTimeRounded}"
            //textViewResult.text = calculator2.fillTimeForContainer.toString()

        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
