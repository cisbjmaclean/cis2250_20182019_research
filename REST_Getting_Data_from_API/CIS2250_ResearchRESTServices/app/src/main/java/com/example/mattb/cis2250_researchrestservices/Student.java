/**
 * Created By: Matt Bryan & Cody Hardy
 * Date: Jan 24, 2019
 * Purpose: Student class containing required student attributes
 */

package com.example.mattb.cis2250_researchrestservices;

public class Student {

      //Student Attributes
    private String id;
    private String username;
    private String phone;
    private String studentId;
    private String lastName;
    private String firstName;
    private String resumeLocation;
    private String preferredTypeOfWork;
    private String coverLetterSubmittedDate;
    private String resumeSubmittedDate;

    //Getters
    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPhone() {
        return phone;
    }

    public String getStudentId() {
        return studentId;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getResumeLocation() {
        return resumeLocation;
    }

    public String getPreferredTypeOfWork() {
        return preferredTypeOfWork;
    }

    public String getCoverLetterSubmittedDate() {
        return coverLetterSubmittedDate;
    }

    public String getResumeSubmittedDate() {
        return resumeSubmittedDate;
    }


}
