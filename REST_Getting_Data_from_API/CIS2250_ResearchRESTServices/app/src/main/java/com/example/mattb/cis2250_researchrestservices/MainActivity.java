package com.example.mattb.cis2250_researchrestservices;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Button and event listener - @MB, @CH
        Button buttonGetStudentInfo = findViewById(R.id.button);
        buttonGetStudentInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new HttpRequestTask().execute();
            }
        });
    }

    // Method to access the API using RestTemplate to make HTTP Rest Calls - @MB, @CH
    private class HttpRequestTask extends AsyncTask<Void, Void, Student> {
        @Override
        protected Student doInBackground(Void... params) {
            try {
                final String url = "http://hccis.info:8080/ojt/rest/StudentService/students/jtownsend@hollandcollege.com";
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                Student student = restTemplate.getForObject(url, Student.class);
                return student;
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }
            return null;
        }

        // Populate Data method
        protected void onPostExecute(Student student) {

            //Assign TextView ID's to variables
            TextView idTextView = findViewById(R.id.textViewID);
            TextView usernameTextView = findViewById(R.id.textViewUName);
            TextView phoneTextView = findViewById(R.id.textViewPhone);
            TextView stuIDTextView = findViewById(R.id.textViewStuID);
            TextView lNameTextView = findViewById(R.id.textViewLName);
            TextView fNameTextView = findViewById(R.id.textViewFName);
            TextView resLocTextView = findViewById(R.id.textViewResLoc);
            TextView PTOWTextView = findViewById(R.id.textViewPTOW);
            TextView covDateTextView = findViewById(R.id.textViewCovDate);
            TextView resDateTextView = findViewById(R.id.textViewResDate);

            //Set text values for variables
            idTextView.setText(student.getId());
            usernameTextView.setText(student.getUsername());
            phoneTextView.setText(student.getPhone());
            stuIDTextView.setText(student.getStudentId());
            lNameTextView.setText(student.getLastName());
            fNameTextView.setText(student.getFirstName());
            resLocTextView.setText(student.getResumeLocation());
            PTOWTextView.setText(student.getPreferredTypeOfWork());
            covDateTextView.setText(student.getCoverLetterSubmittedDate());
            resDateTextView.setText(student.getResumeSubmittedDate());
        }
    }
}
