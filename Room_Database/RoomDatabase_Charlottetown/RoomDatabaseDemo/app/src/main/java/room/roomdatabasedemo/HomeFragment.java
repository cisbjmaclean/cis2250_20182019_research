package room.roomdatabasedemo;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements View.OnClickListener
{
    private Button btnAdd;
    private Button btnDelete;
    private Button btnShow;
    private Button btnUpdate;
    public HomeFragment()
    {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        btnAdd = view.findViewById(R.id.buttonAdd);
        btnDelete = view.findViewById(R.id.buttonDelete);
        btnShow = view.findViewById(R.id.buttonShow);
        btnUpdate = view.findViewById(R.id.buttonUpdate);
        btnAdd.setOnClickListener(this);
        btnShow.setOnClickListener(this);
        btnUpdate.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
        // Inflate the layout for this fragment
        return view;
    }
    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.buttonAdd:
                MainActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container, new AddUserFragment()).addToBackStack(null).commit();
                break;
            case R.id.buttonShow:
                MainActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container, new ViewUserFragment()).addToBackStack(null).commit();
                break;
            case R.id.buttonUpdate:
                MainActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container, new UpdateFragment()).addToBackStack(null).commit();
                break;
            case R.id.buttonDelete:
                MainActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container, new DeleteUserFragment()).addToBackStack(null).commit();
                break;
        }
    }
}