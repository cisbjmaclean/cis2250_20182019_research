package room.roomdatabasedemo;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddUserFragment extends Fragment
{
    private EditText userId, fName, lName;
    private Button addButton;
    public AddUserFragment()
    {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_user, container, false);

        userId = view.findViewById(R.id.textId);
        fName = view.findViewById(R.id.textFNameUpdate);
        lName = view.findViewById(R.id.textLName);
        addButton = view.findViewById(R.id.buttonUpdateUser);

        addButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                /*Add in the local variables as they are entered in.*/
                int UserID = Integer.parseInt(userId.getText().toString());
                String FName = fName.getText().toString();
                String LName = lName.getText().toString();
            }
        });
        return view;
    }
}