package com.example.roomdatabase;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity  {



    public static MyAppDatabase myAppDatabase;


   public static FragmentManager fragmentManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        myAppDatabase = Room.databaseBuilder(getApplicationContext(),MyAppDatabase.class, "userdb").allowMainThreadQueries().build();

        fragmentManager = getSupportFragmentManager();

        if(findViewById(R.id.fragment_container) !=null){

            if(savedInstanceState!=null){
                return;
            }

            fragmentManager.beginTransaction().add(R.id.fragment_container,new HomeFragment()).commit();
        }



    }



    }

