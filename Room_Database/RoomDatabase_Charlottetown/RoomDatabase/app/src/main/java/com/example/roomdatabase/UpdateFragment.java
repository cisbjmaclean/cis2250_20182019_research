package com.example.roomdatabase;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class UpdateFragment extends Fragment {

    private EditText userId, fName, lName;

    private Button btnUpdate;


    public UpdateFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

       View view =  inflater.inflate(R.layout.fragment_update, container, false);

        userId = view.findViewById(R.id.textUpdateId);
        fName = view.findViewById(R.id.textFNameUpdate);
        lName = view.findViewById(R.id.textLNameUpdate);
        btnUpdate = view.findViewById(R.id.buttonUpdateUser);
        // Inflate the layout for this fragment

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = Integer.parseInt(userId.getText().toString());
                String firstName = fName.getText().toString();
                String lastName = lName.getText().toString();

                User user = new User();

                user.setUserId(id);
                user.setfName(firstName);
                user.setlName(lastName);

                MainActivity.myAppDatabase.userDao().updateUser(user);

                Toast.makeText(getActivity(),"User successfully updated",Toast.LENGTH_SHORT).show();
            }
        });



        return view;
    }

}
