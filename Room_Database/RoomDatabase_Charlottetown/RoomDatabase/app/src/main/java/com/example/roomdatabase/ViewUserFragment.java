package com.example.roomdatabase;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ViewUserFragment extends Fragment {

    private TextView txtInfo;
    public ViewUserFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View v = inflater.inflate(R.layout.fragment_view_user, container, false);

        txtInfo = v.findViewById(R.id.textView1);

        List<User> users = MainActivity.myAppDatabase.userDao().getUsers();

        String info = "";

        for(User current: users){

            int id = current.getUserId();
            String fName = current.getfName();
            String lName = current.getlName();

            info = info+"\n\n Id:  "+id+"\n First Name: " + fName+"\n Last Name: " + lName;
        }

        txtInfo.setText(info);

        return v;

    }

}
