package com.example.roomdatabase;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddUserFragment extends Fragment  {

    private EditText userId, fName, lName;


    public AddUserFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_add_user, container, false);

        userId = v.findViewById(R.id.textId);
        fName = v.findViewById(R.id.textFNameUpdate);
        lName = v.findViewById(R.id.textLName);
        Button btnSave = v.findViewById(R.id.buttonUpdateUser);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int id = Integer.parseInt(userId.getText().toString());
                String firstName = fName.getText().toString();
                String lastName = lName.getText().toString();


                User user = new User();
                user.setUserId(id);
                user.setfName(firstName);
                user.setlName(lastName);

                MainActivity.myAppDatabase.userDao().addUser(user);
                Toast.makeText(getActivity(), "User added successfully", Toast.LENGTH_SHORT).show();

                userId.setText("");
                fName.setText("");
                lName.setText("");


            }
        });


        return v;
    }

}
