package info.cis2250hccis.scourtney.databaseroom;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements View.OnClickListener {
    //button variables
    private Button btnAdd;
    private Button btnShow;

    public HomeFragment() {
        // Required empty constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        btnAdd = view.findViewById(R.id.btnAdd);
        btnShow = view.findViewById(R.id.btnShow);

        btnAdd.setOnClickListener(this);
        btnShow.setOnClickListener(this);

        return view;


    }

    @Override
    public void onClick(View view) {

        //Switch to show fragment based on user choice
        switch (view.getId()) {

            case R.id.btnAdd:
                MainActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container, new FragmentUser()).addToBackStack(null).commit();
                break;
            case R.id.btnShow:
                MainActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container, new FragmentView()).addToBackStack(null).commit();

                break;

        }
    }

}
