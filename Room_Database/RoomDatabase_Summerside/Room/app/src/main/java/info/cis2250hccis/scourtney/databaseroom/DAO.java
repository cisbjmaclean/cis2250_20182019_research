package info.cis2250hccis.scourtney.databaseroom;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;
//create you DAO interface
@Dao
public interface DAO {
//put SQL commands here

//insert data into database
@Insert
public void addCamper(CamperRoom camper);

//get items from database
@Query("SELECT * FROM CamperRoom")
public List<CamperRoom> loadAllCampers();


}
