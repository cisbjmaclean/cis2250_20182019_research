package info.cis2250hccis.scourtney.databaseroom;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

//Required annotations. Also added exportSchema property and set to false
@Database(entities = {CamperRoom.class}, version = 3, exportSchema = false)
public abstract class CamperDatabase extends RoomDatabase {

    public abstract DAO dao();
}
