package info.cis2250hccis.scourtney.databaseroom;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class FragmentUser extends Fragment {
    //Screen items/fields
    private EditText id, camper_name;
    private Button BnAdd;
    //private TextView textInfo;

   public FragmentUser(){

   }

   @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
   {
       View view =  inflater.inflate(R.layout.fragment_add_user, container, false);

       //Find id's of the screen items and set the private variables to the screen item ids
       id = view.findViewById(R.id.camper_id);
       camper_name = view.findViewById(R.id.camper_name);
       BnAdd = view.findViewById(R.id.add_button);
       //textInfo = view.findViewById(R.id.camperList);

       //Create onclicklistener for button clicks
       BnAdd.setOnClickListener(new View.OnClickListener(){
           @Override
           public void onClick(View view)
           {
            //Get user entry and set to local variables
            int userid = Integer.parseInt(id.getText().toString());
            String camperName = camper_name.getText().toString();

            //Create new CamperRoom object and set the local variables to the object properties
            CamperRoom camper = new CamperRoom();
            camper.setID(userid);
            camper.setName(camperName);

            //Call the addCamper method from the dao and add a camper to Room database.
            MainActivity.camperDatabase.dao().addCamper(camper);

            //Toast confirming that the camper was added to database
            Toast.makeText(getActivity(), "Camper added!", Toast.LENGTH_SHORT).show();
                //Log.d("TEST", "Did we get here");
                //Clear text inputs for next entry
               id.setText("");
               camper_name.setText("");
         }

       });

       return view;
   }


}
