package info.cis2250hccis.scourtney.databaseroom;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity //here you can add this to change the table name:  (tableName = "newName")
public class CamperRoom {

@PrimaryKey//set the primary key
private int ID;


//;@ColumnInfo(name = "camper_name")//annotation to change column name
private String name;


//your getters and setters
    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
