package info.cis2250hccis.scourtney.databaseroom;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by kgoddard on 2/2/2018.
 */

public class CamperAdapter extends RecyclerView.Adapter<CamperAdapter.MyViewHolder> {
    private List<CamperRoom> campers;
    private Context context;
    private RecyclerView rView;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.camper_row, parent, false);

        Log.d("bjtest", "onCreateViewHolder");
        return new MyViewHolder(itemView);
    }

    public CamperAdapter(Context context, List<CamperRoom> camperList) {
        Log.d("KGADAPTER bjtest", "Instantiating adapter");
        this.context = context;
        this.campers = camperList;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final CamperRoom camper = campers.get(position);
        holder.camperName.setText(camper.getName());
        holder.camperClicked = camper;
    }

    @Override
    public int getItemCount() {
        return campers.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView camperName;
        public CamperRoom camperClicked;
        //public ImageButton expand;

        public MyViewHolder(View view) {
            super(view);
            Log.d("bjtest", "Instantiating MyViewHolder");
            camperName = view.findViewById(R.id.textViewCamperFirstName);

            //Adding a listener to the row.
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("bjtest", "onClick for view of MyViewHolder, row#"
                            +getAdapterPosition()+" camperName="
                            +campers.get(getAdapterPosition()).getName());
                }
            });
        }
    }

}
