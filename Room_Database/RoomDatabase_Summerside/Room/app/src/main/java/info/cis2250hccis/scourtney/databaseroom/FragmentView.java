package info.cis2250hccis.scourtney.databaseroom;




import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentView extends Fragment {

    private TextView textInfo;

    private RecyclerView recyclerView;
    private CamperAdapter camperAdapter;
    private List<CamperRoom> camperList = new ArrayList<CamperRoom>();

    public FragmentView() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_view, container, false);

        //textInfo = v.findViewById(R.id.camperList);
        //make list to hold campers
        camperList = MainActivity.camperDatabase.dao().loadAllCampers();

        //String info = "";
        //iterate list and set to textInfo variable
//        for(CamperRoom camper: campers){
//
//            int id = camper.getID();
//            String name = camper.getName();
//
//            info = info + "\n\n Id:  " + id + "\n Name: " + name + "\n ";
//        }
//
//        textInfo.setText(info);

        return v;

    }

    @Override
    public void onStart() {
        super.onStart();
        //final Bundle tempArgs = getArguments();

        Log.d("bjtest","finished with the background and continuing.");

        recyclerView = getView().findViewById(R.id.recyclerViewCampers);
        camperAdapter = new CamperAdapter(getActivity(), camperList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(camperAdapter);

        //Trigger the background process to get the campers from the service.
        //new HttpRequestTask().execute();




    }

}
