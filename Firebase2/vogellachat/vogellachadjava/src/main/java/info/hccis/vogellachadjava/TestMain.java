package info.hccis.vogellachadjava;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.DatabaseReference.CompletionListener;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bjmaclean
 */
public class TestMain {

    public static void main(String[] args) {

        //Create a list of bookings
        Gson gson = new Gson();

        FileInputStream serviceAccount = null;
        FirebaseApp defaultApp = null;
        try {
            serviceAccount = new FileInputStream("c:/vogellachat/vogellachat-accountkeyjava.json");
            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                    .setDatabaseUrl("https://vogellachat-6af3f.firebaseio.com/")
                    .build();
            defaultApp = FirebaseApp.initializeApp(options);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestMain.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TestMain.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                serviceAccount.close();
            } catch (IOException ex) {
                Logger.getLogger(TestMain.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        // Initialize the default app
        //    = FirebaseApp.initializeApp();
        System.out.println("defaultApp.getName()=" + defaultApp.getName());  // "[DEFAULT]"

// Retrieve services by passing the defaultApp variable...
        FirebaseAuth defaultAuth = FirebaseAuth.getInstance(defaultApp);

        FirebaseDatabase defaultDatabase = FirebaseDatabase.getInstance(defaultApp);
        System.out.println("Default database=" + defaultDatabase.toString());

// ... or use the equivalent shorthand notation
        defaultAuth = FirebaseAuth.getInstance();
        //defaultDatabase = FirebaseDatabase.getInstance();

        DatabaseReference ref = defaultDatabase.getReference("message");

// Attach a listener to read the data at our posts reference
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                System.out.println("----- data changed START -----");

                Object document = dataSnapshot.getValue();
                System.out.println(document);
                
                //Go from location of [ to length -1 of the snapshot.
                String bookingsJsonAll = document.toString();
                int start = bookingsJsonAll.indexOf("[");
                String bookingsJson = bookingsJsonAll.substring(start, bookingsJsonAll.length()-1);
                System.out.println("json="+bookingsJson);
                Booking[] bookingArray = gson.fromJson(bookingsJson, Booking[].class);
                System.out.println("size="+bookingArray.length);
                for(Booking current: bookingArray){
                    System.out.println("-- "+current);
                }

                System.out.println("----- data changed END -----");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

        CompletionListener cl = (DatabaseError de, DatabaseReference dr) -> {
            System.out.println("completed db stuff...");
        };

        //defaultAuth.
        System.out.println("END");

        String menu = "";
        do {
            System.out.println("Enter booking id");
            Scanner input = new Scanner(System.in);
            String bookingIdEntered = input.nextLine();
            if (!bookingIdEntered.equals("0")) {
                Booking newBooking = new Booking();
                newBooking.setBookingId(Integer.parseInt(bookingIdEntered));
                ref.child("bookings").child(bookingIdEntered).setValue(newBooking, cl);
                System.out.println("ref.push();");
                ref.push();
                System.out.println("Added a new booking");
            }
        } while (!menu.equals("0"));

    }

}
