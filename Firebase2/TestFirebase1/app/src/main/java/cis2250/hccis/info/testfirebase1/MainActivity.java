package cis2250.hccis.info.testfirebase1;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final int RC_SIGN_IN = 100;
    private Button buttonAdd;
    private EditText editTextName;
    private EditText editTextNameOpponent;
    private Spinner spinnerCourtId;
    private TextView textViewNumberOfBookings;

    //Firebase objects
    // Write a message to the database
    FirebaseDatabase database;
    DatabaseReference myRef;
    private static int numberOfBookings = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        //Obtain the button add from the view.
        buttonAdd = findViewById(R.id.buttonAdd);
        editTextName = findViewById(R.id.editTextName);
        editTextNameOpponent = findViewById(R.id.editTextNameOpponent);
        spinnerCourtId = findViewById(R.id.spinnerCourtId);
        textViewNumberOfBookings = findViewById(R.id.textViewNumberOfbookings);

        //Set the values for the spinner
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.courts_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinnerCourtId.setAdapter(adapter);


        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("BJM", "clicked the button");
                Log.d("BJM", "editTextName value=" + editTextName.getText().toString());
                Log.d("BJM", "editTextNameOpponent value=" + editTextNameOpponent.getText().toString());
                Log.d("BJM", "editTextName value=" + spinnerCourtId.getSelectedItem().toString());

                //Create and set the values in a booking object
                Booking booking = new Booking();
                booking.setBookingId(numberOfBookings + 1);
                Player player = new Player();
                player.setName(editTextName.getText().toString());
                booking.setPlayer(player);
                Player opponent = new Player();
                opponent.setName((editTextNameOpponent.getText().toString()));
                booking.setOpponent(opponent);
                //Save the new booking to Firebase.
                myRef.child("bookings").child("" + booking.getBookingId()).setValue(booking);

            }
        });


        // Write a message to the database
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("message");

//        Booking booking = new Booking(1, 1, 1);
//        myRef.child("bookings").child("1").setValue(booking);
//
//        Booking booking2 = new Booking(3, 11, 3);
//        myRef.child("bookings").child("3").setValue(booking);

        // Attach a listener to read the data at our posts reference
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                System.out.println("----- data changed START -----");

                Object document = dataSnapshot.getValue();
                System.out.println(document);

                try {
                    //Go from location of [ to length -1 of the snapshot.
                    String bookingsJsonAll = document.toString();
                    int start = bookingsJsonAll.indexOf("[");
                    String bookingsJson = bookingsJsonAll.substring(start, bookingsJsonAll.length() - 1);
                    System.out.println("json=" + bookingsJson);
                    Gson gson = new Gson();
                    Booking[] bookingArray = gson.fromJson(bookingsJson, Booking[].class);
                    System.out.println("size=" + bookingArray.length);
                    numberOfBookings = 0;
                    for (Booking current : bookingArray) {
                        if (current != null) {
                            numberOfBookings++;
                            System.out.println("-- " + current);
                        }
                    }
                    textViewNumberOfBookings.setText("Bookings: "+numberOfBookings);
                } catch (Exception e) {
                    System.out.println("could not load data.");
                }
                System.out.println("----- data changed END -----");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });


//        // Choose authentication providers
//        List<AuthUI.IdpConfig> providers = Arrays.asList(
//                new AuthUI.IdpConfig.EmailBuilder().build());
//
//// Create and launch sign-in intent
//        startActivityForResult(
//                AuthUI.getInstance()
//                        .createSignInIntentBuilder()
//                        .setAvailableProviders(providers)
//                        .build(),
//                RC_SIGN_IN);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("bjtest - auth result", "" + resultCode);


    }

}
