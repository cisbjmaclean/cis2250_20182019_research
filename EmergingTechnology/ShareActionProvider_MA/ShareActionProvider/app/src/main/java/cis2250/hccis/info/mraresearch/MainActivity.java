package cis2250.hccis.info.mraresearch;

import android.Manifest;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pub.devrel.easypermissions.EasyPermissions;

/**
 * This is the main activity of the application
 * Provides user the functionality to share a text, an image, or both
 * This class implements
 *
 * @author  MRAkmel
 * @version 1.0
 * @since   2019-03-21
 */
public class MainActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {

    private Intent shareIntent;
    private EditText textViewToshare;
    private ImageView imgViewToShare;
    private FloatingActionButton btnShare;
    private Button btnAddPhoto;
    private String textToshare;
    private Uri selectedImage;
    private final static int RESULT_LOAD_IMAGE = 1;
    private final static int PICK_IMAGE_REQUEST_CODE = 1000;
    private static final String TAG = MainActivity.class.getSimpleName();

    /**
     * This is method sets the parameters and listeners
     * @param savedInstanceState
     * @author  MRAkmel
     * @version 1.0
     * @since   2019-03-21
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set content
        setContentView(R.layout.activity_main);

        //Get widgets from the view
        textViewToshare = (EditText) findViewById(R.id.textView_toShare);
        imgViewToShare = (ImageView) findViewById(R.id.imgView_toShare);
        btnShare = (FloatingActionButton) findViewById(R.id.fab_share);
        btnAddPhoto = (Button) findViewById(R.id.btn_addPhoto);

        //Set listener for onClick event of Add Photo button
        btnAddPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                //Create an indent to open the gallery
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });

        //Set listener for onClick event of Share floating action button
        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Get the text to share
                textToshare = textViewToshare.getText().toString();

                //Check if either a text or image is there to share
                if(selectedImage==null&&(textToshare.isEmpty() || textToshare==null) ){
                    //Show a toast if nether a text nor an image identified to share and stop action
                    Toast.makeText(MainActivity.this,
                            "Nothing to share!", Toast.LENGTH_LONG).show();
                    return;
                }

                //Create an intent to send
                shareIntent = new Intent(Intent.ACTION_SEND);

                //If a text is entered add it to intent
                if((!textToshare.isEmpty() || textToshare==null)){
                    shareIntent.setType("text/plain");
                    textToshare = textViewToshare.getText().toString();
                    shareIntent.putExtra(Intent.EXTRA_TEXT, textToshare);
                }
                //If an image is selected add it to intent
                if(selectedImage!=null){
                    shareIntent.setType("image/*");
                    shareIntent.putExtra(Intent.EXTRA_STREAM, selectedImage);
                }
                //Start share intent
                startActivityForResult(shareIntent, RESULT_LOAD_IMAGE);
            }
        });

    }

    /**
     * This is method uses EasyPErmissions library to ask necessary permissions from the user.
     * This method has to be implemented to use EasyPermissions interface
     * @param requestCode,
     * @param permissions,
     * @param grantResults,
     * @author  MRAkmel
     * @version 1.0
     * @since   2019-03-21
     */
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, MainActivity.this);
    }
    /**
     * This is method runs when permissions are granted.
     * This method has to be implemented to use EasyPermissions interface
     * @param requestCode
     * @param perms
     * @author  MRAkmel
     * @version 1.0
     * @since   2019-03-21
     */
    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        Log.d(TAG, "Permission has been granted");
        if(selectedImage != null){
            getPic();
        }else{
            EasyPermissions.requestPermissions(this, getString(R.string.read_access_msg), PICK_IMAGE_REQUEST_CODE, Manifest.permission.READ_EXTERNAL_STORAGE);
        }
    }

    /**
     * This is runs when permissions are denied
     * This method has to be implemented to use EasyPermissions interface
     * @param requestCode
     * @param perms
     * * @author  MRAkmel
     * @version 1.0
     * @since   2019-03-21
     */
    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Log.d(TAG, "Permission has been denied");
    }

    /**
     * This method runs when intent results
     * @param requestCode
     * @param resultCode
     * @param data
     * @author  MRAkmel
     * @version 1.0
     * @since   2019-03-21
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            selectedImage = data.getData();
            //If an image is selected and the app has the permission to read the gallery
            if(EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE)){
                //Gets picture
                getPic();
            }else{
                //Asks user to grant necessary permissions
                EasyPermissions.requestPermissions(this, getString(R.string.read_access_msg), PICK_IMAGE_REQUEST_CODE, Manifest.permission.READ_EXTERNAL_STORAGE);
            }
        }
    }

    /**
     *  This method gets the selected image from the gallery
     * @author  MRAkmel
     * @version 1.0
     * @since   2019-03-21
     */
    private void getPic(){
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(selectedImage,
                filePathColumn, null, null, null);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String picturePath = cursor.getString(columnIndex);
        cursor.close();
        imgViewToShare.setImageBitmap(BitmapFactory.decodeFile(picturePath));
    }

}
