package br.com.edrd.callmom.model;

public class Contact {

    private String phone;

    public Contact(final String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }
}
