package br.com.edrd.callmom.controller;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import br.com.edrd.callmom.R;
import br.com.edrd.callmom.model.Contact;
import br.com.edrd.callmom.model.Reminder;
import br.com.edrd.callmom.service.ReminderService;

public class MainActivity extends Activity {

    public static final String NOTIFICATION_INTERVAL_EXTRA = "br.com.edrd.callmom.controller.NOTIFICATION_INTERVAL_EXTRA";
    public static final String CONTACT_PHONE_NUMBER = "br.com.edrd.callmom.controller.CONTACT_PHONE_NUMBER";
    private static final int REQUEST_SELECT_PHONE_NUMBER = 1;
    private Button abrirAgendaContatosButton;
    private EditText editText;
    private Contact contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        abrirAgendaContatosButton = findViewById(R.id.button);
        abrirAgendaContatosButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectContact();
            }
        });

        editText = findViewById(R.id.editText);
        editText.setText(String.valueOf(Reminder.DEFAULT_TIME_IN_MINUTES));
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus && ((EditText) view).getText().toString().isEmpty()) {
                    ((EditText) view).setText(String.valueOf(Reminder.DEFAULT_TIME_IN_MINUTES));
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SELECT_PHONE_NUMBER && resultCode == RESULT_OK) {
            Uri uri = data.getData();

            if (uri == null) {
                Toast.makeText(this, R.string.mensagem_erro_selecionar_contato,
                        Toast.LENGTH_SHORT).show();

                return;
            }

            final String projection[] = {
                    ContactsContract.CommonDataKinds.Phone.NUMBER
            };

            ContentResolver resolver = getContentResolver();

            if (resolver == null) {
                Toast.makeText(this, R.string.mensagem_erro_selecionar_contato,
                        Toast.LENGTH_SHORT).show();

                return;
            }

            Cursor cursor = resolver.query(uri, projection, null, null, null);

            if (cursor == null) {
                Toast.makeText(this, R.string.mensagem_erro_selecionar_contato,
                        Toast.LENGTH_SHORT).show();

                return;
            }

            if (cursor.moveToFirst()) {
                int index = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                String phone = cursor.getString(index);

                cursor.close();

                Reminder reminder = new Reminder(new Contact(phone),
                        Integer.valueOf(editText.getText().toString()));

                final Uri phoneUri = Uri.fromParts("tel", reminder.getContact().getPhone(), null);

                Intent serviceIntent = new Intent(this, ReminderService.class);
                serviceIntent.setData(phoneUri);
                serviceIntent.putExtra(NOTIFICATION_INTERVAL_EXTRA, reminder.getTimeInHours());
                startService(serviceIntent);

                Intent serviceActivityIntent = new Intent(this, ServiceActivity.class);
                serviceActivityIntent.putExtra(NOTIFICATION_INTERVAL_EXTRA, reminder.getTimeInHours());
                serviceActivityIntent.putExtra(CONTACT_PHONE_NUMBER, reminder.getContact().getPhone());
                startActivity(serviceActivityIntent);
            }
        }
    }

    public void selectContact() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_SELECT_PHONE_NUMBER);
        }
    }
}
