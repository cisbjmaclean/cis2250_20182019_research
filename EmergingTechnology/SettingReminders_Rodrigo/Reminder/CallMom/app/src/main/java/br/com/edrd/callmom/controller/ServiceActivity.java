package br.com.edrd.callmom.controller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import br.com.edrd.callmom.R;
import br.com.edrd.callmom.model.Reminder;

public class ServiceActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);

        Intent intent = getIntent();

        if (intent != null) {
            final int countDownInterval = intent.getIntExtra(MainActivity.NOTIFICATION_INTERVAL_EXTRA, Reminder.DEFAULT_TIME_IN_MINUTES);

            String mensagem = getResources().getString(R.string.mensagem_confirmacao_selecao_contato);

            TextView textView4 = findViewById(R.id.textView4);
            textView4.setText(String.format(mensagem, countDownInterval));

            TextView textView5 = findViewById(R.id.textView5);
            textView5.setText(intent.getStringExtra(MainActivity.CONTACT_PHONE_NUMBER));
        }
    }
}
