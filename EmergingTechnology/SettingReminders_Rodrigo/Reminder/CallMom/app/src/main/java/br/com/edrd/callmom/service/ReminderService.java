package br.com.edrd.callmom.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import br.com.edrd.callmom.R;
import br.com.edrd.callmom.controller.MainActivity;
import br.com.edrd.callmom.model.Reminder;

public class ReminderService extends Service {

    private static final String DEFAULT_CHANNEL_ID = "call_mom_group";
    private static final int NOTIFICATION_ID = 10_000;

    private CountDownTimer timer;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        //final int millisecondsPerMinute = 60_000; // 1 minute
        final int millisecondsPerMinute = 1_000; // 1 segundo
        //final int millisecondsPerMinute = 60_000; // 1 minute
        final int countDownInterval = millisecondsPerMinute * intent.getIntExtra(MainActivity.NOTIFICATION_INTERVAL_EXTRA, Reminder.DEFAULT_TIME_IN_MINUTES);

        timer = new CountDownTimer(countDownInterval, countDownInterval) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                remind(intent.getData());
            }
        };

        timer.start();

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (timer != null) {
            timer.cancel();
        }

        NotificationManagerCompat.from(this).cancel(NOTIFICATION_ID);
    }

    public void remind(Uri number) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(DEFAULT_CHANNEL_ID,
                    DEFAULT_CHANNEL_ID, NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(DEFAULT_CHANNEL_ID);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);

            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }

        Intent call = new Intent(Intent.ACTION_DIAL);
        call.setData(number);
        call.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_FROM_BACKGROUND);

        PendingIntent pending = PendingIntent.getActivity(this, 0, call,
                PendingIntent.FLAG_UPDATE_CURRENT);

        String titulo = getResources().getString(R.string.notificacao_titulo);
        String texto = getResources().getString(R.string.notificacao_texto);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, DEFAULT_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notification_icon)
                .setContentTitle(titulo)
                .setContentText(texto)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pending)
                .setAutoCancel(true);

        NotificationManagerCompat.from(this).notify(NOTIFICATION_ID, builder.build());
    }
}
