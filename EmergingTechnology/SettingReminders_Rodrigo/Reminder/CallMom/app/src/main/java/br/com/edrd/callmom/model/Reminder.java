package br.com.edrd.callmom.model;

public class Reminder {

    public static final int DEFAULT_TIME_IN_MINUTES = 30;
    private Contact contact;
    private int timeInHours;

    public Reminder(final Contact contact, final int timeInHours) {
        this.contact = contact;
        this.timeInHours = timeInHours;
    }

    public Contact getContact() {
        return contact;
    }

    public int getTimeInHours() {
        return timeInHours;
    }
}
